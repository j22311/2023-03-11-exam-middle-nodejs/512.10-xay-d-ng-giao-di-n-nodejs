// khai báo thư viện express 
const express = require("express");
const path = require("path");
const { getTimeCurrent, getUrlCurrent } = require("./middlewares/sign-inMiddleware");
// khai báo app 
const app = express();

//khai báo port
const port = 8000;
//hàm middleware cho get "/"
app.get("/",getTimeCurrent,getUrlCurrent,(request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/sign-in1.html"))
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})