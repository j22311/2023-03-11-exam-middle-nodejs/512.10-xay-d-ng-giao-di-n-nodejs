// const { response } = require("express");
// const { request } = require("http");

const getTimeCurrent = (request,response,next) => {
    console.log(new Date());
    
    next();
}

const getUrlCurrent = (request,response,next) => {
    console.log(__dirname);
    next();
}

module.exports = {
    getTimeCurrent,
    getUrlCurrent
}